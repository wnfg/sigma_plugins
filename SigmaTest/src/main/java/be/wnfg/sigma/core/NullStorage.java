package be.wnfg.sigma.core;

/**
 * A storage implementation that is always disconnected and that never executes
 * anything.
 *
 */
public class NullStorage implements Storage
{

	@Override
	public boolean isConnected()
	{
		return false;
	}

	@Override
	public void execute(SqlRunnable runnable)
	{

	}

	@Override
	public void executeSync(SqlRunnable runnable)
	{
		
	}

}
