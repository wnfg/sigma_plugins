package be.wnfg.sigma.protection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.block.BlockMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import be.wnfg.sigma.core.CoreTest;

public class SigmaProtectionTest
{
	private ServerMock server;
	private SigmaProtection plugin;
	private SigmaProtectionAPI api;

	@Before
	public void setUp() throws Exception
	{
		server = MockBukkit.mock();
		CoreTest.loadCore();
		plugin = MockBukkit.load(SigmaProtection.class);
		api = SigmaProtection.getAPI();
	}

	@After
	public void tearDown()
	{
		MockBukkit.unload();
	}

	@Test(expected = IllegalArgumentException.class)
	public void getPlayer_BlockDoesNotExist_Exception()
	{
		api.getPlayer(new BlockMock());
	}

	@Test
	public void setPlayer_PlayerAndBlock_Player()
	{
		Player player = server.addPlayer();
		Block block = new BlockMock();
		api.setPlayer(player, block);
		Player player2 = api.getPlayer(block);
		assertEquals(player, player2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setPlayer_BlockThatExists_Exception()
	{
		Player player = server.addPlayer();
		Block block = new BlockMock();
		api.setPlayer(player, block);
		api.setPlayer(player, block);
	}

	@Test(expected = IllegalArgumentException.class)
	public void removeBlock_BlockThatDoesNotExists_Exception()
	{
		Block block = new BlockMock();
		api.removeBlock(block);
	}

	@Test
	public void removeBlock_BlockThatExists_Void()
	{
		Player player = server.addPlayer();
		Block block = new BlockMock();
		api.setPlayer(player, block);
		api.removeBlock(block);
		assertFalse(api.isPlacedByPlayer(block));
	}

	@Test
	public void isPlacedByPlayer_BlockThatIsPlaced_True()
	{
		Player player = server.addPlayer();
		Block block = new BlockMock();
		api.setPlayer(player, block);
		assertTrue(api.isPlacedByPlayer(block));
	}

	@Test
	public void isPlacedByPlayer_BlockThatIsNotPlaced_False()
	{
		Block block = new BlockMock();
		assertFalse(api.isPlacedByPlayer(block));
	}

	@Test
	public void onBlockDamage_NotPlacedByPlayer_EventCancelled()
	{
		PlayerMock player = server.addPlayer();
		Block block = new BlockMock();
		assertFalse(player.simulateBlockBreak(block));
		assertFalse(api.isPlacedByPlayer(block));
	}
	
	@Test
	public void onBlockBreak_NotPlacedByPlayerInCreative_EventCancelled()
	{
		PlayerMock player = server.addPlayer();
		player.setGameMode(GameMode.CREATIVE);
		Block block = new BlockMock();
		assertFalse(player.simulateBlockBreak(block));
		assertFalse(api.isPlacedByPlayer(block));
	}

	@Test
	public void onBlockDamage_PlacedByPlayer_EventNotCancelled()
	{
		PlayerMock player = server.addPlayer();
		Block block = new BlockMock();
		BlockPlaceEvent placeEvent = new BlockPlaceEvent(block, null, null, null, player, true, null);
		server.getPluginManager().callEvent(placeEvent);
		assertTrue("Player could not damage block", player.simulateBlockDamage(block));
		assertFalse(api.isPlacedByPlayer(block));
	}

	@Test
	public void onBlockDamage_AlreadyCanceled_Nothing()
	{
		Player player = server.addPlayer();
		Block block = new BlockMock();
		ItemStack itemInHand = null;
		BlockDamageEvent event = new BlockDamageEvent(player, block, itemInHand, false);
		event.setCancelled(true);
		server.getPluginManager().callEvent(event);
		assertTrue(event.isCancelled());
		assertFalse(api.isPlacedByPlayer(block));
	}

	@Test
	public void onBlockPlace_PlacedByPlayer_EventNotCancelled()
	{
		Player player = server.addPlayer();
		Block block = new BlockMock();
		BlockPlaceEvent placeEvent = new BlockPlaceEvent(block, null, null, null, player, true, null);
		server.getPluginManager().callEvent(placeEvent);
		assertTrue(api.isPlacedByPlayer(block));
	}

	@Test
	public void onBlockPlace_EventIsCannceled_EventCancelled()
	{
		Player player = server.addPlayer();
		Block block = new BlockMock();
		BlockPlaceEvent placeEvent = new BlockPlaceEvent(block, null, null, null, player, true, null);
		placeEvent.setCancelled(true);
		Bukkit.getPluginManager().callEvent(placeEvent);
		assertFalse(api.isPlacedByPlayer(block));
	}
	
	@Test
	public void isPlacedByPlayer_PlacedByPlayerInSurvivalThenRemoved_False()
	{
		PlayerMock player = server.addPlayer();
		player.setGameMode(GameMode.SURVIVAL);
		Block block = new BlockMock();
		BlockPlaceEvent placeEvent = new BlockPlaceEvent(block, null, null, null, player, true, null);
		Bukkit.getPluginManager().callEvent(placeEvent);
		assumeTrue(player.simulateBlockBreak(block));
		assertFalse(api.isPlacedByPlayer(block));
	}
	
	@Test
	public void isPlacedByPlayer_PlacedByPlayerInCreativeThenRemoved_False()
	{
		PlayerMock player = server.addPlayer();
		player.setGameMode(GameMode.CREATIVE);
		Block block = new BlockMock();
		BlockPlaceEvent placeEvent = new BlockPlaceEvent(block, null, null, null, player, true, null);
		Bukkit.getPluginManager().callEvent(placeEvent);
		assumeTrue(player.simulateBlockBreak(block));
		assertFalse(api.isPlacedByPlayer(block));
	}

	@Test
	public void onDisable_Void_AllBlocksCleared()
	{
		Block block = new BlockMock(Material.BOOKSHELF);
		Player player = server.addPlayer();
		BlockPlaceEvent placeEvent = new BlockPlaceEvent(block, null, null, null, player, true, null);
		Bukkit.getPluginManager().callEvent(placeEvent);
		server.getPluginManager().disablePlugin(plugin);
		assertEquals(Material.AIR, block.getType());
	}
}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
