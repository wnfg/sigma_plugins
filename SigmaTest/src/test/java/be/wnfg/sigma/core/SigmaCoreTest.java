package be.wnfg.sigma.core;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;

public class SigmaCoreTest
{
	private ServerMock server;
	private SigmaCore plugin;

	public static SigmaCore loadCore()
	{
		return MockBukkit.load(SigmaCore.class, new NullStorage());
	}
	
	@Before
	public void setUp() throws Exception
	{
		server = MockBukkit.mock();
		plugin = loadCore();
	}
	
	@After
	public void tearDown()
	{
		MockBukkit.unload();
	}

	@Test
	public void givePlayerAdminTool_PlayerDoesNotHaveAdminTool_PlayerHasAdminTool()
	{
		PlayerMock player = server.addPlayer();
		plugin.givePlayerAdminTool(player);
		assertTrue(plugin.isAdminTool(player.getInventory().getItem(0)));
	}
	
	@Test
	public void takePlayerAdminTool_PlayerHasAdminTool_PlayerDoesNotHaveAdminTool()
	{
		PlayerMock player = server.addPlayer();
		plugin.givePlayerAdminTool(player);
		assumeTrue(plugin.isAdminTool(player.getInventory().getItem(0)));
		plugin.takePlayerAdminTool(player);
		assertFalse(plugin.isAdminTool(player.getInventory().getItem(0)));
	}
	
	@Test
	public void isPlayerHoldingAdminTool_PlayerIsNotHoldingAdminTool_False()
	{
		PlayerMock player = server.addPlayer();
		plugin.givePlayerAdminTool(player);
		assumeTrue(plugin.isAdminTool(player.getInventory().getItem(0)));
		player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
		assertFalse(plugin.isPlayerHoldingAdminTool(player));
	}
	
	@Test
	public void isPlayerHoldingAdminTool_PlayerIsHoldingAdminTool_True()
	{
		PlayerMock player = server.addPlayer();
		plugin.givePlayerAdminTool(player);
		assumeTrue(plugin.isAdminTool(player.getInventory().getItem(0)));
		player.getInventory().setItemInMainHand(player.getInventory().getItem(0));
		assertTrue(plugin.isPlayerHoldingAdminTool(player));
	}
	
	@Test
	public void isAdminTool_IsAdminTool_True()
	{
		ItemStack adminTool = new ItemStack(Material.DIAMOND_HOE);
		ItemMeta meta = adminTool.getItemMeta();
		meta.setDisplayName(SigmaCore.TOOL_NAME);
		adminTool.setItemMeta(meta);
		adminTool.setAmount(4);
		assertTrue(plugin.isAdminTool(adminTool));
	}
	
	@Test
	public void isAdminTool_MaterialWrong_False()
	{
		ItemStack adminTool = new ItemStack(Material.GOLD_HOE);
		ItemMeta meta = adminTool.getItemMeta();
		meta.setDisplayName(SigmaCore.TOOL_NAME);
		adminTool.setItemMeta(meta);
		assertFalse(plugin.isAdminTool(adminTool));
	}
	
	@Test
	public void isAdminTool_DisplayNameWrong_False()
	{
		ItemStack adminTool = new ItemStack(Material.DIAMOND_HOE);
		ItemMeta meta = adminTool.getItemMeta();
		meta.setDisplayName("Other Name");
		adminTool.setItemMeta(meta);
		assertFalse(plugin.isAdminTool(adminTool));
	}
	
	@Test
	public void isAdminTool_NoItemMeta_False()
	{
		ItemStack adminTool = new ItemStack(Material.DIAMOND_HOE);
		assertFalse(plugin.isAdminTool(adminTool));
	}
	
	@Test
	public void isAdminTool_Null_False()
	{
		assertFalse(plugin.isAdminTool(null));
	}

}

















