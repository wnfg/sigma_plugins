package be.wnfg.sigma.menus;

import static org.junit.Assert.*;

import java.util.concurrent.atomic.AtomicBoolean;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;

public class SigmaButtonTest
{
	@SuppressWarnings("unused")
	private ServerMock server;
	@SuppressWarnings("unused")
	private SigmaMenus plugin;

	@Before
	public void setUp() throws Exception
	{
		server = MockBukkit.mock();
		plugin = MockBukkit.load(SigmaMenus.class);
	}
	
	@After
	public void tearDown()
	{
		MockBukkit.unload();
	}
	
	@Test
	public void constructor_GettersFilled_PluginReturned()
	{
		ItemStack itemstack = new ItemStack(Material.WOOD);
		Runnable action = () -> {};
		SigmaButton button = new SigmaButton(plugin, action, itemstack);
		assertSame("Plugin was not set by constructor", plugin, button.getPlugin());
		assertSame("Action was not set by constructor", action, button.getAction());
		assertSame("ItemStack was not set by constructor", itemstack, button.getItemStack());
	}
	
	@Test(expected = NullPointerException.class)
	public void constructor_PluginIsNull_Exception()
	{
		ItemStack itemstack = new ItemStack(Material.WOOD);
		Runnable action = () -> {};
		new SigmaButton(null, action, itemstack);
	}
	
	@Test(expected = NullPointerException.class)
	public void constructor_ActionIsNull_Exception()
	{
		ItemStack itemstack = new ItemStack(Material.WOOD);
		new SigmaButton(plugin, null, itemstack);
	}
	
	@Test(expected = NullPointerException.class)
	public void constructor_ItemStackIsNull_Exception()
	{
		Runnable action = () -> {};
		new SigmaButton(plugin, action, null);
	}
	
	@Test
	public void click_RunnableAndPluginIsEnabled_GetsExecuted()
	{
		AtomicBoolean b = new AtomicBoolean();
		SigmaButton button = new SigmaButton(plugin, () -> {
			b.set(true);
		}, new ItemStack(Material.WOOD));
		button.click();
		assertTrue("Button action was not executed", b.get());
	}
	
	@Test
	public void click_RunnableButPluginIsDisabled_NotExecuted()
	{
		AtomicBoolean b = new AtomicBoolean();
		server.getPluginManager().disablePlugin(plugin);
		SigmaButton button = new SigmaButton(plugin, () -> {
			b.set(true);
		}, new ItemStack(Material.WOOD));
		button.click();
		assertFalse("Button action was executed", b.get());
	}
	
}





















