package be.wnfg.sigma.economy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;

public class BankAccountTest
{
	@SuppressWarnings("unused")
	private ServerMock server;
	private SigmaEconomy plugin;

	@Before
	public void setUp() throws Exception
	{
		server = MockBukkit.mock();
		plugin = MockBukkit.load(SigmaEconomy.class);
	}

	@After
	public void tearDown()
	{
		MockBukkit.unload();
	}

	@Test
	public void giveMoney_Money_MoneyAdded()
	{
		BankAccount account = new BankAccount(plugin);
		assumeNotNull(account);
		account.giveMoney(20);
		long amount = account.getAmount();
		assertEquals(20, amount);
	}

	@Test
	public void takeMoney_Money_MoneyRemoved()
	{
		BankAccount account = new BankAccount(plugin);
		assumeNotNull(account);
		account.giveMoney(20);
		account.takeMoney(11);
		long amount = account.getAmount();
		assertEquals(9, amount);
	}

	@Test(expected = IllegalArgumentException.class)
	public void giveMoney_NegativeMoney_Exeption()
	{
		BankAccount account = new BankAccount(plugin);
		assumeNotNull(account);
		account.giveMoney(-20);
	}

	@Test(expected = IllegalArgumentException.class)
	public void takeMoney_NegativeMoney_Exeption()
	{
		BankAccount account = new BankAccount(plugin);
		assumeNotNull(account);
		account.giveMoney(20);
		account.takeMoney(-10);
	}

	@Test
	public void transferMoneyTo_Money_TotalMoney()
	{
		BankAccount account1 = new BankAccount(plugin);
		assumeNotNull(account1);
		BankAccount account2 = new BankAccount(plugin);
		assumeNotNull(account2);

		account1.addAmount(120);
		account1.transferMoneyTo(account2, 20);
		assertEquals(20, account2.getAmount());
		assertEquals(100, account1.getAmount());
	}

	@Test(expected = NotEnoughMoneyExeption.class)
	public void transferMoneyTo_TooMuchMoney_Exeption()
	{
		BankAccount account1 = new BankAccount(plugin);
		assumeNotNull(account1);
		BankAccount account2 = new BankAccount(plugin);
		assumeNotNull(account2);

		account1.addAmount(120);
		account2.transferMoneyTo(account2, 500);
	}

	@Test
	public void addAmount_Money_MoneyAdded()
	{
		BankAccount account = new BankAccount(plugin);
		assumeNotNull(account);
		account.addAmount(20);
		long amount = account.getAmount();
		assertEquals(20, amount);
	}

	@Test
	public void takeAmount_Money_MoneyRemoved()
	{
		BankAccount account = new BankAccount(plugin);
		assumeNotNull(account);
		account.addAmount(20);
		account.takeAmount(11);
		long amount = account.getAmount();
		assertEquals(9, amount);
	}

	@Test(expected = NotEnoughMoneyExeption.class)
	public void takeMoney_NotEnoughMoney_MoneyRemoved()
	{
		BankAccount account = new BankAccount(plugin);
		assumeNotNull(account);
		account.giveMoney(10);
		account.takeMoney(110);
	}

	@Test
	public void setAmount_Amount_Amount()
	{
		BankAccount account = new BankAccount(plugin);
		assumeNotNull(account);
		account.setAmount(20);
		account.setAmount(11);
		long amount = account.getAmount();
		assertEquals(11, amount);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setAmount_NegativeAmount_Exeption()
	{
		BankAccount account = new BankAccount(plugin);
		assumeNotNull(account);
		account.setAmount(-20);
	}
}
