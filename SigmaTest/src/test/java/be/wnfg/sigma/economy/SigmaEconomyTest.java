package be.wnfg.sigma.economy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assume.assumeNotNull;

import org.bukkit.entity.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;

public class SigmaEconomyTest
{
	private ServerMock server;
	private SigmaEconomy plugin;

	@Before
	public void setUp() throws Exception
	{
		server = MockBukkit.mock();
		plugin = MockBukkit.load(SigmaEconomy.class);
	}

	@After
	public void tearDown()
	{
		MockBukkit.unload();
	}

	@Test
	public void getAccount_Player_BankAccount()
	{
		Player player = server.addPlayer();
		BankAccount account = plugin.getAccount(player);
		BankAccount account2 = plugin.getAccount(player);
		assertNotNull(account);
		assertEquals(account, account2);
	}

	@Test
	public void getAccount_String_BankAccount()
	{
		BankAccount account1 = plugin.getAccount("abc");
		BankAccount account2 = plugin.getAccount("abc");
		BankAccount account3 = plugin.getAccount("def");
		assertNotNull(account1);
		assertEquals(account1, account2);
		assertFalse(account1.equals(account3));
	}

	@Test
	public void getMoneyAdded_Money_TotalMoney()
	{
		BankAccount account1 = plugin.getAccount("abc");
		BankAccount account2 = plugin.getAccount("def");
		assumeNotNull(account1);
		assumeNotNull(account2);
		account1.giveMoney(500);
		account2.giveMoney(20);
		account1.takeMoney(500);
		assertEquals(520, plugin.getMoneyAdded());
	}

	@Test(expected = IllegalArgumentException.class)
	public void getMoneyAdded_NegativeMoney_Exeption()
	{
		BankAccount account1 = plugin.getAccount("abc");
		assumeNotNull(account1);
		account1.giveMoney(-500);
	}

	@Test
	public void getDeleted_Money_TotalDeletedMoney()
	{
		BankAccount account1 = plugin.getAccount("abc");
		BankAccount account2 = plugin.getAccount("def");
		assumeNotNull(account1);
		assumeNotNull(account2);
		account1.giveMoney(500);
		account2.giveMoney(20);
		account1.takeMoney(500);
		assertEquals(500, plugin.getDeleted());
	}

	@Test(expected = IllegalArgumentException.class)
	public void getDeleted_NegativeMoney_Exeption()
	{
		BankAccount account1 = plugin.getAccount("abc");
		assumeNotNull(account1);
		account1.takeMoney(-500);
	}

	@Test
	public void getInflation_Money_InflationCount()
	{
		BankAccount account1 = plugin.getAccount("abc");
		BankAccount account2 = plugin.getAccount("def");
		assumeNotNull(account1);
		assumeNotNull(account2);
		account1.giveMoney(500);
		account2.giveMoney(20);
		account1.takeMoney(500);
		assertEquals(20, plugin.getInflation());
	}

	@Test
	public void getInflationRate_Money_InflationCount()
	{
		BankAccount account1 = plugin.getAccount("abc");
		BankAccount account2 = plugin.getAccount("def");
		assumeNotNull(account1);
		assumeNotNull(account2);
		account1.giveMoney(500);
		account2.giveMoney(500);
		account1.takeMoney(500);
		assertEquals(2, plugin.getInflationRate(), 0.001);
	}

	@Test
	public void addInflation_Money_MoreInflation()
	{
		plugin.addInflation(15);
		plugin.addInflation(5);
		assertEquals(20, plugin.getInflation());
	}

	@Test
	public void removeInflation_Money_LessInflation()
	{
		plugin.removeInflation(15);
		plugin.removeInflation(5);
		assertEquals(-20, plugin.getInflation());
	}

	@Test(expected = IllegalArgumentException.class)
	public void addInflation_NegativeMoney_Exeption()
	{
		plugin.addInflation(-10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void removeInflation_NegativeMoney_Exeptopn()
	{
		plugin.removeInflation(-10);
	}
}
