package be.wnfg.sigma.core;

import org.bukkit.entity.Player;

/**
 * A player wrapper that adds several core Sigma features to a player, such as
 * names.
 */
public interface SigmaPlayer
{
	/**
	 * Gets the player this wrapper is wrapping.
	 * 
	 * @return The player this wrapper is wrapping.
	 */
	public Player getPlayer();

	/**
	 * Gets the id of the player. This id can be used to reference the player in
	 * a database.
	 * 
	 * @return The id of the player.
	 */
	public int getId();

	/**
	 * Gets the first name of the player.
	 * 
	 * @return The first name of the player.
	 */
	public String getFirstName();

	/**
	 * Gets the last name of the player.
	 * 
	 * @return The last name of the player.
	 */
	public String getLastName();
	
	/**
	 * Gets the name of the player, if the player data has been loaded.
	 * @return The name of the player, {@code null} if it hasn't been loaded yet.
	 */
	public String getName();

	/**
	 * Sets the name of the player.
	 * 
	 * @param firstName The new first name to give to the player.
	 * @param lastName The new last name to give to the player.
	 */
	public void setName(String firstName, String lastName);

}
