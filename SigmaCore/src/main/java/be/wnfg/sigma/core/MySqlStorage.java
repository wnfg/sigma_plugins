package be.wnfg.sigma.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import org.bukkit.Bukkit;

public class MySqlStorage implements Storage, Runnable
{
	private BlockingQueue<SqlRunnable> tasks = new LinkedBlockingQueue<>();
	private SigmaCore plugin;
	private Logger logger;
	private Connection connection;
	private boolean connected = false;

	public MySqlStorage(SigmaCore core)
	{
		plugin = core;
		logger = core.getLogger();
	}

	/**
	 * Connects to the database.
	 */
	protected void connect(String host, String db, String username, String password)
	{
		try
		{
			logger.info("Connecting to database " + username);
			Class.forName("com.mysql.jdbc.Driver");
			String url = String.format("jdbc:mysql://%s/%s?user=%s&password=%s", host, db, username, password);
			connection = DriverManager.getConnection(url);
			connected = true;
			logger.info("Connected to database");
			StorageReadyEvent event = new StorageReadyEvent();

			Bukkit.getScheduler().runTaskAsynchronously(plugin, this);
			Bukkit.getScheduler().runTask(plugin, () ->
			{
				Bukkit.getPluginManager().callEvent(event);
			});
		}
		catch (ClassNotFoundException | SQLException e)
		{
			logger.severe("Could not connect to MySql database: " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void run()
	{
		while (connected)
		{
			try
			{
				tasks.take().run(connection);
			}
			catch (InterruptedException e)
			{
			}
			catch (Exception e)
			{
				logger.severe("Failed to execute SqlRunnable: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public boolean isConnected()
	{
		return connected;
	}

	@Override
	public void execute(SqlRunnable runnable)
	{
		while (true)
		{
			try
			{
				tasks.put(runnable);
				return;
			}
			catch (InterruptedException e)
			{
			}
		}
	}
	
	@Override
	public void executeSync(SqlRunnable runnable)
	{
		try
		{
			runnable.run(connection);
		}
		catch (SQLException e)
		{
			logger.severe("Failed to execute SqlRunnable: " + e.getMessage());
			throw new RuntimeException(e);
		}
	}

}
