package be.wnfg.sigma.core;

public interface Storage
{
	/**
	 * Checks if there is currently an active connection with the database. Note
	 * that the return value only gets updated during initialization or after a
	 * task was executed.
	 * 
	 * @return {@code true} if the database is connected, {@code false} if it
	 *         isn't.
	 */
	public boolean isConnected();

	/**
	 * Executes a task synchronized to the storage medium.
	 * 
	 * @param runnable The task to run.
	 */
	public void execute(SqlRunnable runnable);
	
	/**
	 * Executes a task on the callers thread.
	 * 
	 * @param runnable The task to run.
	 */
	public void executeSync(SqlRunnable runnable);
}
