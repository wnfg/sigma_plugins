package be.wnfg.sigma.core;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class StorageReadyEvent extends Event
{
	private static HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers()
	{
		return handlers;
	}
	
	public static HandlerList getHandlerList()
	{
		return handlers;
	}

}
