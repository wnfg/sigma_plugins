package be.wnfg.sigma.core;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface SigmaCoreAPI
{
	/**
	 * Gives the player an admin tool.
	 * @param player The player to whom an admin tool should be given.
	 */
	public void givePlayerAdminTool(Player player);
	
	/**
	 * Checks if the player is currently holding an admin tool in their hand.
	 * @param player The player to check for.
	 * @return {@code true} if the player is holding an admin tool, {@code false} if they're not. 
	 */
	public boolean isPlayerHoldingAdminTool(Player player);
	
	/**
	 * Takes away the admin tool from a player.
	 * The player does not need to be holding the item in his hand.
	 * @param player The player from whom to take the admin tool.
	 */
	public void takePlayerAdminTool(Player player);
	
	/**
	 * Checks if the given {@link ItemStack} is an admin tool.
	 * @param item The item to check for.
	 * @return {@code true} if the tool is an admin tool, {@code false} if it isn't.
	 */
	public boolean isAdminTool(ItemStack item);
	
	/**
	 * Gets the current storage API.
	 * Each plugin can have several databases it can access.
	 * @return An object that can be used to communicate with the storage device.
	 */
	public Storage getStorage();
}
