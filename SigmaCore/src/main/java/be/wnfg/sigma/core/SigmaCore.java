package be.wnfg.sigma.core;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

public class SigmaCore extends JavaPlugin implements SigmaCoreAPI
{
	/**
	 * The name of the admin tool as seen in the player's inventory. 
	 */
	public static final String TOOL_NAME = "Admin Scythe";
	private static SigmaCoreAPI api = null;
	private ItemStack adminTool;
	private Storage storage;
	
	public SigmaCore()
	{
		super();
		storage = new MySqlStorage(this);
	}
	
	protected SigmaCore(final JavaPluginLoader loader, final PluginDescriptionFile description, final File dataFolder, final File file, Storage storage)
	{
		super(loader, description, dataFolder, file);
		this.storage = storage;
	}
	
	public static SigmaCoreAPI getAPI()
	{
		return api;
	}
	
	/**
	 * Converts a UUID into a byte array.
	 * @param uuid The UUID to convert.
	 * @return The UUID as a byte array.
	 */
	public static byte[] toBytes(UUID uuid)
	{
		ByteBuffer buffer = ByteBuffer.wrap(new byte[16]);
		buffer.putLong(uuid.getMostSignificantBits());
		buffer.putLong(uuid.getLeastSignificantBits());
		return buffer.array();
	}
	
	/**
	 * Converts a player's UUID to a byte array.
	 * @param player The player whose UUID to convert.
	 * @return The UUID converted to a byte array.
	 */
	public static byte[] toUUIDBytes(Player player)
	{
		return toBytes(player.getUniqueId());
	}
	
	@Override
	public void onLoad()
	{
		api = this;
		
		adminTool = new ItemStack(Material.DIAMOND_HOE);
		ItemMeta meta = adminTool.getItemMeta();
		meta.setDisplayName(TOOL_NAME);
		adminTool.setItemMeta(meta);
	}
	
	@Override
	public void onEnable()
	{
		saveDefaultConfig();
		String host = getConfig().getString("db_host");
		String db = getConfig().getString("db_name");
		String username = getConfig().getString("db_user");
		String password = getConfig().getString("db_pass");
		if (storage instanceof MySqlStorage)
			((MySqlStorage) storage).connect(host, db, username, password);
	}

	@Override
	public void givePlayerAdminTool(Player player)
	{
		player.getInventory().addItem(adminTool);
	}

	@Override
	public boolean isPlayerHoldingAdminTool(Player player)
	{
		ItemStack item = player.getInventory().getItemInMainHand();
		return isAdminTool(item);
	}

	@Override
	public void takePlayerAdminTool(Player player)
	{
		PlayerInventory inventory = player.getInventory();
		ItemStack[] stacks = inventory.getContents();
		for (ItemStack item : stacks)
		{
			if (isAdminTool(item))
				item.setType(Material.AIR);
		}
		inventory.setContents(stacks);
	}

	@Override
	public boolean isAdminTool(ItemStack item)
	{
		return adminTool.isSimilar(item);
	}
	
	@Override
	public Storage getStorage()
	{
		return storage;
	}
}
































