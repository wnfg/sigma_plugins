package be.wnfg.sigma.core;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * A functional interface that executes a query on a SQL Database.
 */
@FunctionalInterface
public interface SqlRunnable
{
	/**
	 * Runs when the query is executed on the database.
	 * @param connection The connection on which the SQL statements can be sent.
	 * @throws SQLException when a SQL query fails.
	 */
	public void run(Connection connection) throws SQLException;
}
