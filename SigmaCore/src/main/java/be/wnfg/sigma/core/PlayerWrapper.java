package be.wnfg.sigma.core;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.entity.Player;

/**
 * A player wrapper that adds several core Sigma features to a player, such as
 * names.
 */
public class PlayerWrapper
{
	private final Storage storage;
	private final Player player;
	private int id = -1;
	private String firstName;
	private String lastName;

	/**
	 * Creates a new player wrapper for a specific player.
	 * 
	 * @param player The player to wrap.
	 */
	protected PlayerWrapper(Storage storage, Player player)
	{
		this.storage = storage;
		this.player = player;
		load();
	}

	/**
	 * Checks if the player information has already been loaded from the
	 * database.
	 * 
	 * @return {@code true} if the player information has already been loaded,
	 *         {@code false} if it hasn't been loaded yet.
	 */
	private boolean isLoaded()
	{
		return id != -1;
	}

	/**
	 * Tries to load the data from the database.
	 */
	private void load()
	{
		storage.execute(conn -> {
			synchronized (this)
			{
				PreparedStatement statement = conn
						.prepareStatement("SELECT player_id, first_name, last_name FROM players WHERE player_uuid=?");
				statement.setBytes(1, SigmaCore.toUUIDBytes(player));
				ResultSet set = statement.executeQuery();
				if (set.next())
				{
					id = set.getInt("player_id");
					firstName = set.getString("first_name");
					lastName = set.getString("last_name");
				}
			}
		});
	}

	/**
	 * Updates the player in the database if needed. If the player needs no
	 * update, nothing is done.
	 */
	private void update()
	{
		storage.execute(conn ->
		{
			synchronized (this)
			{
				if (isLoaded())
				{
					PreparedStatement statement = conn
							.prepareStatement("UPDATE players (first_name, last_name) VALUES (?, ?) WHERE player_id = ?");
					statement.setString(1, firstName);
					statement.setString(2, lastName);
					statement.setInt(3, id);
					statement.executeUpdate();
				}
			}
		});
	}

	/**
	 * Gets the player this wrapper is wrapping.
	 * 
	 * @return The player this wrapper is wrapping.
	 */
	public Player getPlayer()
	{
		return player;
	}

	/**
	 * Gets the id of the player. This id can be used to reference the player in
	 * a database.
	 * 
	 * @return The id of the player.
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * Gets the first name of the player.
	 * 
	 * @return The first name of the player.
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * Gets the last name of the player.
	 * 
	 * @return The last name of the player.
	 */
	public String getLastName()
	{
		return lastName;
	}
	
	/**
	 * Gets the name of the player, if the player data has been loaded.
	 * @return The name of the player, {@code null} if it hasn't been loaded yet.
	 */
	public String getName()
	{
		if (firstName != null)
			return firstName + " " + lastName;
		else
			return null;
	}

	/**
	 * Sets the name of the player.
	 * 
	 * @param firstName The new first name to give to the player.
	 * @param lastName The new last name to give to the player.
	 */
	public synchronized void setName(String firstName, String lastName)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		update();
	}

}
