/**
 * A set of common methods that every plugin can safely depend on.
 * 
 * These methods are put into a single plugin because they both don't fit into
 * any other plugin and are too small or trivial to be put into their own
 * plugin.
 */
package be.wnfg.sigma.core;