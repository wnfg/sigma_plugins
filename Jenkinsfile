pipeline {
  agent any
  triggers {
    pollSCM('')
    bitbucketPush()
  }
  
  stages {
    stage('Build') {
      steps {
        sh './gradlew jar'
      }
    }
    stage('Test') {
      steps {
        sh './gradlew test jacocoTestReport'
        junit '**/test-results/**/*.xml'
        jacoco(classPattern: '**/classes', execPattern: '**/**.exec', sourcePattern: '**/src/main/java')
      }
    }
    stage('Publish') {
      parallel {
        stage('Archive') {
          steps {
            archiveArtifacts(artifacts: '*/build/libs/*.jar', excludes: '**/SigmaTest.jar')
          }
        }
        stage('Publish To Maven') {
		  when {
			  expression { return env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master' }
		  }
          environment {
            MVN = credentials('ruska-artifactory')
            MVN_URL = 'http://10.0.1.5:8081/artifactory/libs-release-local/'
          }
          steps {
            sh "./gradlew publish -Pmvnuser=${env.MVN_USR} -Pmvnpass=${env.MVN_PSW} -Pmvnurl=${env.MVN_URL}"
          }
        }
      }
    }
	stage('Deploy Testing') {
	  when {
	  	  expression { return env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master' }
	  }
      steps {
        sshagent (credentials: ['grensvilletest_rsa']) {
          sh './gradlew storeArtifacts'
		  sh "ssh grensville_test rm -f /root/minecraft/plugins/Sigma*.jar"
          sh 'scp build/artifacts/* grensville_test:/root/minecraft/plugins'
        }
      }
	}
	stage('Deploy Production') {
		when {
			branch 'master'
		}
		steps {
			sshagent (credentials: ['grensvilletest_rsa']) {
				sh './gradlew storeArtifacts'
		  		sh "ssh grensville rm -f /root/minecraft/plugins/Sigma*.jar"
				sh 'scp build/artifacts/* grensville_test:/root/minecraft/plugins'
			}	
		}
	}
  }
}
