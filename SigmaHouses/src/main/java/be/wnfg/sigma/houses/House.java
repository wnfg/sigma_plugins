package be.wnfg.sigma.houses;

import java.util.Set;

import org.bukkit.block.Block;

/**
 * Represents a house that can be bought by players.
 */
public class House
{
	
	/**
	 * Gets the price of the house.
	 * @return The price of the house.
	 */
	public long getPrice()
	{
		return 0;
	}
	
	/**
	 * Sets the price of the house.
	 * @param price The new of the house.
	 */
	public void setPrice(long price)
	{
		
	}
	
	/**
	 * Gets the name of the house.
	 * @return The name of the house.
	 */
	public String getName()
	{
		return null;
	}
	
	/**
	 * Sets the name of the house.
	 * @param name The new name of the house.
	 */
	public void setName(String name)
	{
		
	}
	
	/**
	 * Gets all door blocks that are part of house.
	 * @return A set of all door blocks of the house.
	 */
	public Set<Block> getDoors()
	{
		return null;
	}
	
	/**
	 * Adds a door block to the house.
	 * @param door
	 */
	public void addDoor(Block door)
	{
		
	}
	
	/**
	 * Removes a door block from the house.
	 * @param door The door block to remove.
	 */
	public void removeDoor(Block door)
	{
		
	}
	
	/**
	 * Adds an information sign that a player can use to buy the house.
	 * @param sign The sign to add to the house.
	 */
	public void addSign(Block sign)
	{
		
	}
	
	/**
	 * Removes an information sign that a player can use to buy the house.
	 * @param sign The sign to remove from the house.
	 */
	public void removeSign(Block sign)
	{
		
	}
	
	/**
	 * Gets a set of information signs that a player can use to buy the house.
	 * @return A set of information signs that are part of the house.
	 */
	public Set<Block> getSigns()
	{
		return null;
	}
	
}

























