package be.wnfg.sigma.houses;

import java.util.Set;

public interface SigmaHousesAPI
{
	/**
	 * Gets a house.
	 * @param houseName The name of the house to get.
	 * @return The house.
	 */
	public House getHouse(String houseName);
	
	/**
	 * Creates a house.
	 * @param houseName The name of the house to get.
	 * @return The newly created houses.
	 */
	public House createHouse(String houseName);
	
	/**
	 * Checks if a house with the given name exist.
	 * @param houseName The name of the house to check for.
	 * @return {@code true} if the house exists, {@code false} if it doesn't.
	 */
	public boolean houseExists(String houseName);
	
	/**
	 * Gets a set of all houses in the game.
	 * @return The set of all houses that exist.
	 */
	public Set<House> getHouses();
}
