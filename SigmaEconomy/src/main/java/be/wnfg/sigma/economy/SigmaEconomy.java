package be.wnfg.sigma.economy;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

public class SigmaEconomy extends JavaPlugin implements SigmaEconomyAPI
{
	private static SigmaEconomyAPI api;
	private long inflationPool = 0;
	private long inflationMoneyAdded = 0;
	public long inflationMoneyRemoved = 0;
	private Map<String, BankAccount> accountList = new LinkedHashMap<>();

	public SigmaEconomy()
	{
		super();
	}

	protected SigmaEconomy(final JavaPluginLoader loader, final PluginDescriptionFile description,
			final File dataFolder, final File file)
	{
		super(loader, description, dataFolder, file);
	}

	public static SigmaEconomyAPI getAPI()
	{
		return api;
	}

	@Override
	public void onLoad()
	{
		api = this;
	}

	@Override
	public void onEnable()
	{
		// TODO Stub
	}

	@Override
	public void onDisable()
	{
		// TODO Stub
	}

	@Override
	public BankAccount getAccount(Player player)
	{
		return getAccount(player.getUniqueId().toString());
	}

	@Override
	public BankAccount getAccount(String name)
	{
		if (accountList.containsKey(name))
		{
			return accountList.get(name);
		}
		else
		{
			BankAccount account = new BankAccount((SigmaEconomy) api);
			accountList.put(name, account);
			return account;
		}
	}

	@Override
	public long getMoneyAdded()
	{
		return inflationMoneyAdded;
	}

	@Override
	public long getDeleted()
	{
		return inflationMoneyRemoved;
	}

	@Override
	public long getInflation()
	{
		return inflationPool;
	}

	@Override
	public double getInflationRate()
	{
		return ((double) inflationMoneyAdded) / ((double) inflationMoneyRemoved);
	}

	@Override
	public void addInflation(long amount)
	{
		if (amount < 0)
			throw new IllegalArgumentException("Amount can't be negative but is " + amount);

		inflationPool += amount;
		inflationMoneyAdded += amount;
	}

	@Override
	public void removeInflation(long amount)
	{
		if (amount < 0)
			throw new IllegalArgumentException("Amount can't be negative but is " + amount);

		inflationPool -= amount;
		inflationMoneyRemoved += amount;
	}
}
