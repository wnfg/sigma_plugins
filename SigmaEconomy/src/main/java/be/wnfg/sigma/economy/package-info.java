/**
 * A set of economy related methods that a plugin can use to control player's bank accounts.
 */
package be.wnfg.sigma.economy;