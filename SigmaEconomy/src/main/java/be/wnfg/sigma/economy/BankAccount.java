package be.wnfg.sigma.economy;

/**
 * Represents a bank account that can contain a certain amount of money.
 * 
 * @author seeseemelk
 */
public class BankAccount
{
	private SigmaEconomy plugin;
	private long amount = 0;

	/**
	 * Creates a new bank account.
	 */
	protected BankAccount(SigmaEconomy plugin)
	{
		this.plugin = plugin;
	}

	/**
	 * Gets the amount of money that is on the bank account.
	 * 
	 * @return The amount of money that is on the bank account.
	 */
	public long getAmount()
	{
		return amount;
	}

	/**
	 * Adds money to the bank account. This counts towards the money added to the
	 * game.
	 * 
	 * @param amount The amount to add to it.
	 */
	public void giveMoney(long amount)
	{
		if (amount < 0)
			throw new IllegalArgumentException("Amount hat to be at least 0 but is " + amount);
		plugin.addInflation(amount);
		this.addAmount(amount);
	}

	/**
	 * Takes money from the bank account. This counts towards the money removed from
	 * the game.
	 * 
	 * @param amount The amount to remove from it.
	 */
	public void takeMoney(long amount)
	{
		if (amount > this.amount)
			throw new NotEnoughMoneyExeption(
					"The account only has " + this.amount + "$ and tries to pay " + amount + "$");

		plugin.removeInflation(amount);
		this.takeAmount(amount);
	}

	/**
	 * Transfers money from this bank account to a different one.
	 * 
	 * @param target The target account, to which money will be deposited.
	 * @param amount The amount of money to transfer.
	 */
	public void transferMoneyTo(BankAccount target, long amount)
	{
		if (amount > this.amount)
			throw new NotEnoughMoneyExeption(
					"The account only has " + this.amount + "$ and tries to pay " + amount + "$");

		this.amount -= amount;
		target.addAmount(amount);
	}

	/**
	 * Sets the amount of money that is on the bank account.
	 * 
	 * @param amount The new amount of money that is on the bank account.
	 */
	protected void setAmount(long amount)
	{
		if (amount < 0)
			throw new IllegalArgumentException("Amount has to be at least 0 but is" + amount);
		this.amount = amount;
	}

	/**
	 * Adds an amount of money to the bank account.
	 * 
	 * @param amount The amount of money to add to the bank account.
	 */
	protected void addAmount(long amount)
	{
		this.amount += amount;
	}

	/**
	 * Removes an amount of money from the bank account.
	 * 
	 * @param amount The amount of money to remove from the bank account.
	 */
	protected void takeAmount(long amount)
	{
		addAmount(-amount);
	}
}
