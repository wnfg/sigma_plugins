package be.wnfg.sigma.economy;

public class NotEnoughMoneyExeption extends IllegalArgumentException
{
	private static final long serialVersionUID = 1L;

	/*
	 * public NotEnoughMoneyExeption() { }
	 */

	public NotEnoughMoneyExeption(String arg0)
	{
		super(arg0);
	}

	/*
	 * public NotEnoughMoneyExeption(Throwable arg0) { super(arg0); }
	 */

	/*
	 * public NotEnoughMoneyExeption(String arg0, Throwable arg1) { super(arg0,
	 * arg1); }
	 */

}
