package be.wnfg.sigma.economy;

import org.bukkit.entity.Player;

public interface SigmaEconomyAPI
{
	/**
	 * Gets a bank account of a player.
	 * 
	 * @param player
	 *            The player from whom to get the bank account.
	 * @return The bank account for a the player. Creates one if none exist.
	 */
	public BankAccount getAccount(Player player);
	
	/**
	 * Gets a bank account for any string. Note that this string isn't the same as
	 * the name of the user, but it can be used to reference an NPC bank account if
	 * a consistent name is used.
	 * 
	 * @param name
	 *            The name of the bank account that should be retrieved.
	 * @return The bank account. Creates one if none exist.
	 */
	public BankAccount getAccount(String name);
	
	/**
	 * Gets the total amount of money that was added to the game.
	 * 
	 * @return The total amount of money that was added to the game.
	 */
	public long getMoneyAdded();
	
	/**
	 * Gets the total amount of money that was removed from the game.
	 * 
	 * @return The total amount of money that was removed from the game.
	 */
	public long getDeleted();
	
	/**
	 * Gets the how much money has been added to the game after subtracting the
	 * amount of money that was removed.
	 * 
	 * @return The amount of money that was added after subtracting the amount
	 *         removed.
	 */
	public long getInflation();
	
	/**
	 * Gets the rate of inflation, e.g.: how many units of money were added per unit
	 * of money removed.
	 * 
	 * @return The amount of money added per unit of money removed.
	 */
	public double getInflationRate();
	

	/**
	 * Adds money to the total inflation pool.
	 * @param amount The amount of money to add.
	 */
	public void addInflation(long amount);
	
	/**
	 * Removes money to the total inflation pool
	 * @param amount The amount of money to remove.
	 */
	public void removeInflation(long amount);
}
