package be.wnfg.sigma.items;

import org.bukkit.Material;

/**
 * Represents any sort of custom item that can be added to the game.
 *
 */
public abstract class CustomItem
{

	/**
	 * Creates a custom with the the given name and material type.
	 * @param name The name of the custom item.
	 * @param material The material icon the item should have.
	 */
	protected CustomItem(String name, Material material)
	{
		
	}
	
	/**
	 * Checks if this custom item can stack on another one.
	 * @param item The other item to stack this one onto.
	 * @return {@code true} if the items can stack, {@code false} if they can't.
	 */
	public boolean canStack(CustomItem item)
	{
		return equals(item);
	}

}
