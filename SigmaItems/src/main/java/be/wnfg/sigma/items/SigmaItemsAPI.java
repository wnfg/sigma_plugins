package be.wnfg.sigma.items;

import org.bukkit.entity.Player;

public interface SigmaItemsAPI
{
	/**
	 * Tries to give an item to the player.
	 * @param player The player to give the item to.
	 * @param item The item to give.
	 * @return The amount of the stack that couldn't be given to the player.
	 */
	public int giveItem(Player player, CustomItem item);
}
