package be.wnfg.sigma.items;

import java.io.File;

import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

public class SigmaItems extends JavaPlugin implements SigmaItemsAPI
{
	private static SigmaItemsAPI api;
	
	public SigmaItems()
	{
		super();
	}
	
	protected SigmaItems(final JavaPluginLoader loader, final PluginDescriptionFile description, final File dataFolder, final File file)
	{
		super(loader, description, dataFolder, file);
	}
	
	public static SigmaItemsAPI getAPI()
	{
		return api;
	}
	
	@Override
	public void onLoad()
	{
		api = this;
	}
	
	@Override
	public void onEnable()
	{
		// TODO Stub
	}
	
	@Override
	public void onDisable()
	{
		// TODO Stub
	}

	@Override
	public int giveItem(Player player, CustomItem item)
	{
		return 0;
	}
}
































