package be.wnfg.sigma.menus;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * A button that can be shown in a {@link SigmaMenu}.
 * When pressed a {@link Runnable} will be executed.
 */
public class SigmaButton
{
	private final Plugin plugin;
	private final Runnable action;
	private final ItemStack itemstack;
	
	/**
	 * Creates a new {@code SigmaButton}.
	 * @param plugin The plugin that created the button.
	 * @param action The action to execute when the button is pressed.
	 * @param itemstack The itemstack that should be shown in the menu.
	 */
	public SigmaButton(Plugin plugin, Runnable action, ItemStack itemstack)
	{
		if (plugin == null)
			throw new NullPointerException("plugin is null");
		else if (action == null)
			throw new NullPointerException("action is null");
		else if (itemstack == null)
			throw new NullPointerException("itemstack is null");
		this.plugin = plugin;
		this.action = action;
		this.itemstack = itemstack;
	}
	
	/**
	 * Gets the plugin that creates this button.
	 * @return The plugin that created this button.
	 */
	public Plugin getPlugin()
	{
		return plugin;
	}
	
	/**
	 * Gets the action that will be executed when the button is pressed.
	 * @return The action that will be executed when the button is pressed.
	 */
	public Runnable getAction()
	{
		return action;
	}
	
	/**
	 * Gets the {@link ItemStack} that will be used to display in a menu.
	 * Note that modifying this itemstack might not change the button in the menu.
	 * @return An {@link ItemStack} representing the button in a menu.
	 */
	public ItemStack getItemStack()
	{
		return itemstack;
	}

	/**
	 * Performs a click on this button.
	 */
	public void click()
	{
		if (plugin.isEnabled())
		{
			action.run();
		}
	}
	
}
