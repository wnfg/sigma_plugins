package be.wnfg.sigma.protection;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public interface SigmaProtectionAPI
{
	/**
	 * Gets the {@link Player} that placed a certain {@link Block}.
	 * 
	 * @param block The {@link Block} you want to check.
	 * @return The {@link Player} that placed the {@link Block}.
	 */
	public Player getPlayer(Block block);

	/**
	 * Adds a new {@link Player} / {@link Block} entry.
	 * 
	 * @param player The {@link Player} that placed the {@link Block}.
	 * @param block The {@link Block} that the {@link Player} placed.
	 */
	public void setPlayer(Player player, Block block);

	/**
	 * Removes a {@link Block}.
	 * 
	 * @param block The {@link Block} that should be removed.
	 */
	public void removeBlock(Block block);

	/**
	 * Checks i a {@link Block} is placed by a {@link Player} or is just part of the
	 * map.
	 * 
	 * @param block The {@link Block} you want to check.
	 * @return {@code true} if the {@link Block} is placed by a player,
	 *         {@code false} otherwise.
	 */
	public boolean isPlacedByPlayer(Block block);
}
