package be.wnfg.sigma.protection;

import java.io.File;
import java.sql.PreparedStatement;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

import be.wnfg.sigma.core.SigmaCore;
import be.wnfg.sigma.core.SigmaCoreAPI;

public class SigmaProtection extends JavaPlugin implements SigmaProtectionAPI, Listener
{
	private Map<Block, Player> blockPlayerList = new LinkedHashMap<>();
	private SigmaCoreAPI core;
	private static SigmaProtectionAPI api;

	public SigmaProtection()
	{
		super();
	}

	protected SigmaProtection(final JavaPluginLoader loader, final PluginDescriptionFile description,
			final File dataFolder, final File file)
	{
		super(loader, description, dataFolder, file);
	}

	public static SigmaProtectionAPI getAPI()
	{
		return api;
	}

	@Override
	public void onDisable()
	{
		removeAllBlocks();
	}

	@Override
	public void onLoad()
	{
		api = this;
		core = SigmaCore.getAPI();
	}

	@Override
	public void onEnable()
	{
		Bukkit.getPluginManager().registerEvents(this, this);
	}

	/**
	 * Removes all blocks placed by players.
	 */
	private void removeAllBlocks()
	{
		for (Block b : blockPlayerList.keySet())
		{
			b.setType(Material.AIR);
		}
		blockPlayerList.clear();
	}

	@Override
	public Player getPlayer(Block block)
	{
		if (!blockPlayerList.containsKey(block))
			throw new IllegalArgumentException("This block is not placed by a player or does not exist");

		return blockPlayerList.get(block);
	}

	@Override
	public void setPlayer(Player player, Block block)
	{
		if (blockPlayerList.containsKey(block))
			throw new IllegalArgumentException("This is already placed by " + blockPlayerList.get(block).getName());

		blockPlayerList.put(block, player);

		core.getStorage().execute(conn ->
		{
			PreparedStatement statement = conn
					.prepareStatement("INSERT INTO protection (block_x, block_y, block_z, player) VALUES (?, ?, ?, ?)");
			statement.setInt(1, block.getX());
			statement.setInt(2, block.getY());
			statement.setInt(3, block.getZ());
			statement.setBytes(4, SigmaCore.toUUIDBytes(player));
			statement.executeUpdate();
		});
	}

	@Override
	public void removeBlock(Block block)
	{
		if (!blockPlayerList.containsKey(block))
			throw new IllegalArgumentException("This block does not exist");

		blockPlayerList.remove(block);
		
		core.getStorage().execute(conn ->
		{
			PreparedStatement statement = conn
					.prepareStatement("DELETE FROM protection WHERE block_x = ? AND block_y = ? AND block_z = ?");
			statement.setInt(1, block.getX());
			statement.setInt(2, block.getY());
			statement.setInt(3, block.getZ());
			statement.executeUpdate();
		});
	}

	@Override
	public boolean isPlacedByPlayer(Block block)
	{
		return blockPlayerList.containsKey(block);
	}

	@EventHandler
	public void onBlockDamage(BlockDamageEvent event)
	{
		if (event.isCancelled())
			return;
		if (!isPlacedByPlayer(event.getBlock()))
			event.setCancelled(true);
		else
			removeBlock(event.getBlock());
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event)
	{
		if (event.isCancelled())
			return;
		if (!isPlacedByPlayer(event.getBlock()))
			event.setCancelled(true);
		else
			removeBlock(event.getBlock());
	}

	@EventHandler
	public void onBlockPlaceEvent(BlockPlaceEvent event)
	{
		if (event.isCancelled())
			return;
		Player player = event.getPlayer();
		Block block = event.getBlock();
		setPlayer(player, block);
	}
}
